//
//  ViewController.swift
//  Dafiti Project
//
//  Created by Igor Henrique Pessoa Aguiar on 15/01/16.
//  Copyright © 2016 IHPA. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet var Product:UITextField!
    @IBOutlet var Logo:UIImageView!
    @IBOutlet var listaProdutos:UITableView!
    
    
    
    @IBAction func busca(){
        let procurar = Product.text
        Product.resignFirstResponder();
        
        startConnection(procurar!)
    }
    
    @IBAction func atualizar(){
        
        startConnection()
    }
    
    
    func startConnection(item: String){
        var urlPath: String = "http://api.bestbuy.com/v1/products(name="
        urlPath .appendContentsOf(item)
        let restoURL: String = "?format=json&show=sku,name,salePrice&apiKey=h24z9nt9f2ajdjktbt7d4zc6"
        urlPath.appendContentsOf(restoURL)
        
        let url: NSURL = NSURL(string: urlPath)!
        let request: NSURLRequest = NSURLRequest(URL: url)
        let connection: NSURLConnection = NSURLConnection(request: request, delegate: self, startImmediately: false)!
        connection.start()
    }
    
    func startConnection(){
        var urlPath: String = "http://api.bestbuy.com/v1/products(name=canon?format=json&show=sku,name,salePrice&apiKey=h24z9nt9f2ajdjktbt7d4zc6"
        
        let url: NSURL = NSURL(string: urlPath)!
        let request: NSURLRequest = NSURLRequest(URL: url)
        let connection: NSURLConnection = NSURLConnection(request: request, delegate: self, startImmediately: false)!
        connection.start()
    }

//    func getJSON(urlToRequest: String) -> NSData{
//        return NSData(contentsOfURL: NSURL(string: urlToRequest)!)!
//    }
//    
//    
//    func parseJSON(inputData: NSData) -> NSDictionary{
//        var error: NSError?
//        let boardsDictionary: NSDictionary = NSJSONSerialization.JSONObjectWithData(inputData, options: NSJSONReadingOptions.MutableContainers, error: &error) as NSDictionary
//        
//        return boardsDictionary
//    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        Product.delegate=self;
        
        
    }
    
    func textFieldShouldReturn(textField: UITextField) ->Bool {
        Product.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

